export default () => <div className='home_index_title_container'>
    <style jsx>{`
    .home_index_title_container img{
        margin-top: 70px;
        font-family: HYWenHei-GEW;
        font-size: 42px;
        color: #222;
        line-height: 42px;
        text-align: center;

    }
    .home_index_title_container>p{
        margin-bottom: 70px;
    }
    .home_index_title_container p{
        font-family: PingFangSC-Regular;
        font-size: 22px;
        color: #666;
        text-align: center;
        line-height: 40px;
    }`}</style>
    <div style={{ textAlign: "center" }}>
        <img src="https://static.lufaxcdn.com/lufaxholding/pages/lufax/images/home_index_title_img.a6887aa1.png" alt="" style={{ width: "250px", height: "46px", }} />
    </div>
    <p>让零售信贷和财富管理更简单，更安全，更高效。</p>
</div>
import { Menu } from 'antd';
import 'antd/dist/antd.css';
import TITLE from '../components/Banner'
export default () => <div>
    <style jsx>{`
        *{
            margin: 0;
            padding: 0;
        }
        header{
            width: 100%;
            min-width: 1200px;
            height: 80px;
            position: sticky;
            top: 0;
            z-index:3;
            background: #fff;
         }
         header>div{
            width: 1200px;
            height: 100%;
            margin: 0 auto;
            display: flex;
            align-items: center;
            justify-content: space-between;
         }
        h1 img{
            width: 104px;
            height: 36px;
        }
        .banner{
            position: relative;
        }
        .img{
            position: absolute;
            left: 50%;
            top:50%;
            margin-left: -428px;
            margin-top: -87px;
        }
        .home_index_block_content_container{
            position: relative;
            min-width: 1100px;
            padding-left: 80px;
            margin: 0 50px;
            height: 410px;
            background: #fff;
            box-shadow: 0 10px 30px 0 rgb(64 61 136 / 10%);
        }
        .home_index_block_content_subtitle {
            margin-top: 20px;
            font-family: PingFangSC-Regular;
            font-size: 20px;
            color: #666;
            letter-spacing: .39px;
            line-height: 30px;
        }
        .home_img{
            width: 631px;
            height: 496px;
            position: absolute;
            right: 0;
            top: -40px;
        }
    `}</style>
    <header>
        <div>
            <h1><img src="https://static.lufaxcdn.com/lufaxholding/common/images/logo.80d5324a.jpg" alt="" /></h1>
            <Menu mode="horizontal" style={{ height: "80px", lineHeight: '80px' }}>
                <Menu.Item key="mail">首页</Menu.Item>
                <Menu.Item key="guan">关于我们</Menu.Item>
                <Menu.Item key="ye">业务与服务</Menu.Item>
                <Menu.Item key="xin">新闻中心</Menu.Item>
                <Menu.Item key="tou">投资者关系</Menu.Item>
                <Menu.Item key="lian">联系我们</Menu.Item>
                <Menu.Item key="jia">加入我们</Menu.Item>
                <Menu.Item key="jian">简/EN</Menu.Item>
            </Menu >
        </div>
    </header>
    <div className='banner'>
        <video src="https://ljs1.wmcn.com/static/upload/video/20201026/16037095810705273.mp4" playsinline="true" webkit-playsinline="true" airplay="allow" x-webkit-airplay="true" style={{ width: '100%' }} autoplay preload loop muted></video>
        <img className='img' src="https://static.lufaxcdn.com/lufaxholding/pages/lufax/images/home_index_video_title.3688842d.png" alt="" style={{ width: "856px", height: "174px" }} />
    </div>
    {/* <div className='home_index_title_container'>
        <div style={{ textAlign: "center" }}>
            <img src="https://static.lufaxcdn.com/lufaxholding/pages/lufax/images/home_index_title_img.a6887aa1.png" alt="" style={{ width: "250px", height: "46px", }} />
        </div>
        <p>让零售信贷和财富管理更简单，更安全，更高效。</p>
    </div> */}
    <TITLE />
    <div className='home_index_block_content_container'>
        <img src="https://static.lufaxcdn.com/lufaxholding/pages/lufax/images/index_errand_1_title.e6ae3db3.png" alt="" style={{ width: "520px", height: "80px", marginTop: "80px" }} />
        <p className='home_index_block_content_subtitle' style={{ width: "420px" }}>
            基于深度学习和大数据分析应用，平台支持自动化组合投资工具，制定定制化且匹配投资者风险偏好的投资组合选择，能通过差异化和自动化投资助力提升投资回报。
        </p>
        <img src="https://static.lufaxcdn.com/lufaxholding/pages/lufax/images/index_errand_1.d29f1660.png" alt="" className='home_img' />
    </div>
</div>